safe3city
=========
##_safe3city_ leads you the safe way.
Project consist of two parts:

* a web application allows to watch dangerous places and plan your trip to avoid them
* an Android application will be your personal guide which warn you of potential danger

Dokumentacja DAO: web/doc

![funkcjonalnosc](https://bitbucket.org/rekrutacja_rekrutacja/projekt-zespolowy-bezpieczne-trojmiasto/raw/master/images/funkcjonalnosc.png)
![projekt wygladu strony](https://bitbucket.org/rekrutacja_rekrutacja/projekt-zespolowy-bezpieczne-trojmiasto/raw/master/images/projekt_wygladu_strony.png)
![aplikacja mobilna](https://bitbucket.org/rekrutacja_rekrutacja/projekt-zespolowy-bezpieczne-trojmiasto/raw/master/images/aplikacja_mobilna.png)
![komunikacja](https://bitbucket.org/rekrutacja_rekrutacja/projekt-zespolowy-bezpieczne-trojmiasto/raw/master/images/komunikacja.png)
![aktorzy](https://bitbucket.org/rekrutacja_rekrutacja/projekt-zespolowy-bezpieczne-trojmiasto/raw/master/images/aktorzy.png)