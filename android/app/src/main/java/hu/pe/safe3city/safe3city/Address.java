package hu.pe.safe3city.safe3city;

import com.google.android.gms.maps.model.LatLng;

public class Address {

    private final Integer id;
    private final String street;
    private final String houseNumber;
    private final String postalCode;
    private final String city;
    private final Double latitude;
    private final Double longitude;


    public Address(Integer id, String street, String houseNumber, String postalCode, String city, Double latitude, Double longitude) {
        this.id = id;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public LatLng getLocation() {
        return new LatLng(this.latitude, this.longitude);
    }
}
