package hu.pe.safe3city.safe3city.menu.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import hu.pe.safe3city.safe3city.R;
import hu.pe.safe3city.safe3city.menu.helpers.DrawerItemCustomAdapter;
import hu.pe.safe3city.safe3city.menu.helpers.ObjectDrawerItem;


//TODO: this works fine but could be designed better (make class with array list of menu fragments)
public class NavigationDrawerFragment extends Fragment {

    public static final String PREF_FILE_NAME = "preferenceFile";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private View containerView;
    private String[] navigationDrawerItemTitles;
    private ListView drawerList;

    private boolean userLearnedDrawer;
    private boolean fromSavedInstanceState;

    public NavigationDrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userLearnedDrawer = readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, false);
        fromSavedInstanceState = savedInstanceState != null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        this.drawerLayout = drawerLayout;
        drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset < 0.6) {
                    toolbar.setAlpha(1 - slideOffset);
                }
            }
        };

        navigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        drawerList = (ListView) getView().findViewById(R.id.drawer_menu_items);

        ArrayList<ObjectDrawerItem> drawerItems = new ArrayList<>();

        drawerItems.add(new ObjectDrawerItem(R.drawable.ic_action_map, getString(R.string.map)));
        drawerItems.add(new ObjectDrawerItem(R.drawable.ic_action_new, getString(R.string.add_incident)));
        drawerItems.add(new ObjectDrawerItem(R.drawable.ic_action_settings, getString(R.string.settings)));
        drawerItems.add(new ObjectDrawerItem(R.drawable.navigation, getString(R.string.route)));
        drawerItems.add(new ObjectDrawerItem(R.drawable.ic_action_error, getString(R.string.get_help)));

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(getActivity(), R.layout.menu_item_row, drawerItems);

        if (adapter != null && drawerList != null)
            drawerList.setAdapter(adapter);

        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        if (!userLearnedDrawer && !fromSavedInstanceState) {
            userLearnedDrawer = true;
            saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, userLearnedDrawer);
            drawerLayout.openDrawer(containerView);
        }

        this.drawerLayout.setDrawerListener(drawerToggle);
        this.drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context context, String preferenceName, Boolean preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferenceName, preferenceValue).apply();
    }

    public static Boolean readFromPreferences(Context context, String preferenceName, Boolean defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(preferenceName, defaultValue);
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    public void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new UserMapFragment();
                break;
            case 1:
                if(isLoggedIn(getActivity())) {
                    fragment = new AddIncidentFragment();
                } else {
                    fragment = new LoginFragment();
                }
                break;
            case 2:
                fragment = new SettingsFragment();
                break;
            case 3:
                fragment = new ChooseRouteFragment();
                break;
            case 4:
                fragment = new GetHelpFragment();
                break;
            case 5:
                fragment = new AddIncidentFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            drawerList.setItemChecked(position, true);
            drawerList.setSelection(position);
            ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(navigationDrawerItemTitles[position]);

            drawerLayout.closeDrawer(containerView);

        } else {
            Log.e("MainActivity", "Error when creating fragment");
        }
    }

    public static Boolean isLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LoginFragment.LOGIN_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.contains("userID");
    }
}
