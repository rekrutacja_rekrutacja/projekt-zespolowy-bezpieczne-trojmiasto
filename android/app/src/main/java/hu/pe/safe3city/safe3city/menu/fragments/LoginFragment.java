package hu.pe.safe3city.safe3city.menu.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import hu.pe.safe3city.safe3city.R;

public class LoginFragment extends android.support.v4.app.Fragment {

    public static final String USER_NAME = "login";
    public static final String USER_PASS = "password";
    public static String LOGIN_FILE_NAME = "loginData";
    private String userName;
    private String pass;
    private View rootView;

    public LoginFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.login_fragment, container, false);
        TextView signUpText = (TextView) rootView.findViewById(R.id.signUpTextView);
        signUpText.setMovementMethod(LinkMovementMethod.getInstance());

        Button signInButton = (Button) rootView.findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignInButtonCLick();
            }
        });

        return rootView;
    }

    private void onSignInButtonCLick() {
        EditText userNameEdit = (EditText) rootView.findViewById(R.id.userNameEdit);
        EditText passEdit = (EditText) rootView.findViewById(R.id.userPasswordEdit);

        userName = userNameEdit.getText().toString();
        pass = passEdit.getText().toString();

        sendJson(userName, pass);
    }


    private void sendJson(String userName, String pass) {
        JSONObject json = new JSONObject();
        try {
            json.put(USER_NAME, userName);
            json.put(USER_PASS, pass);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                "http://188.226.171.110/api/api.php/login", json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("login req: ", jsonObject.toString());
                try {
                    String jStatus = jsonObject.getString("status");

                    switch (jStatus) {
                        case "OK": {
                            JSONObject jData = jsonObject.getJSONObject("data");
                            int userID = jData.getInt("id");
                            String accountType = jData.getString("accountType");
                            logUserIn(userID, accountType);

                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment fragment = new AddIncidentFragment();
                            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        }
                            break;
                        case "NOT_FUND":
                            Toast.makeText(getActivity(), "Wrong data. Check username and password", Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Toast.makeText(getActivity(), "Wrong server response. Contact with administrator",
                                    Toast.LENGTH_LONG).show();
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Wrong server response. Contact with administrator", Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });

        queue.add(request);
    }

    private void logUserIn(Integer userID, String accountType) {
        saveToPreferences(getActivity(), "userID", String.valueOf(userID));
        saveToPreferences(getActivity(), "accountType", String.valueOf(accountType));
    }

    private static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LOGIN_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue).apply();
    }

}
