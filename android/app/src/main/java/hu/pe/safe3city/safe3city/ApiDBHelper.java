package hu.pe.safe3city.safe3city;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class ApiDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "ApiDatabase.db";

    public ApiDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(buildAddressTable());
        db.execSQL(buildIncidentTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists address");
        db.execSQL("drop table if exists incident");
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private String buildAddressTable() {

        return "create table address ( " +
                "id integer primary key, " +
                "street text, " +
                "houseNumber text, " +
                "postalCode text, " +
                "city text, " +
                "latitude real, " +
                "longitude real);";
    }

    private String buildIncidentTable() {

        return "create table incident ( " +
                "id integer primary key, " +
                "userID integer, " +
                "addressID integer references address on delete cascade, " +
                "date text, " +
                "time text, " +
                "victimSex text, " +
                "victimAge integer, " +
                "incidentType text);";
    }

    public void updateAllAddresses(ArrayList<Address> addresses) {
        StringBuilder ids = new StringBuilder();
        for (Address address : addresses) {
            addAddress(address);
            ids.append(address.getId() + ", ");
        }
        ids.delete(ids.length()-2, ids.length()-1);
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM address WHERE id NOT IN ("+ids.toString()+");");
        db.close();
    }

    public void addAddress(Address address) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", address.getId());
        values.put("street", address.getStreet());
        values.put("houseNumber", address.getHouseNumber());
        values.put("postalCode", address.getPostalCode());
        values.put("city", address.getCity());
        values.put("latitude", address.getLatitude());
        values.put("longitude", address.getLongitude());

        String args[] = {String.valueOf(address.getId())};
        if (db.update("address", values, "id=?", args) == 0) {
            db.insert("address", null, values);
        }
    }

    public ArrayList<Address> getAllAddresses() {
        ArrayList<Address> addresses = new ArrayList<>();
        String columns[] = {"id", "street", "houseNumber", "postalCode", "city", "latitude", "longitude"};

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("address", columns, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(0);
            String street = cursor.getString(1);
            String houseNumber = cursor.getString(2);
            String postalCode = cursor.getString(3);
            String city = cursor.getString(4);
            Double latitude = cursor.getDouble(5);
            Double longitude = cursor.getDouble(6);

            Address address = new Address(id, street, houseNumber, postalCode, city, latitude, longitude);
            addresses.add(address);
        }
        cursor.close();

        return addresses;
    }

    private Address getAddressById(Integer addressID) {

        SQLiteDatabase db = getReadableDatabase();
        String columns[] = {"id", "street", "houseNumber", "postalCode", "city", "latitude", "longitude"};

        String args[] = {String.valueOf(addressID)};

        Cursor cursor = db.query("address", columns, "id=?", args, null, null, null);

        if (cursor.moveToNext()) {
            Integer id = cursor.getInt(0);
            String street = cursor.getString(1);
            String houseNumber = cursor.getString(2);
            String postalCode = cursor.getString(3);
            String city = cursor.getString(4);
            Double latitude = cursor.getDouble(5);
            Double longitude = cursor.getDouble(6);

            Address address = new Address(id, street, houseNumber, postalCode, city, latitude, longitude);
            cursor.close();
            return address;
        }
        cursor.close();
        return null;
    }

    public void updateAllIncidents(ArrayList<Incident> incidents) {
        StringBuilder ids = new StringBuilder();
        for (Incident incident : incidents) {
            addIncident(incident);
            ids.append(incident.getId() + ", ");
        }
        ids.delete(ids.length()-2, ids.length()-1);
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM incident WHERE id NOT IN ("+ids.toString()+");");
        db.close();
    }

    public void addIncident(Incident incident) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", incident.getId());
        values.put("userID", incident.getUserID());
        values.put("addressID", incident.getAddress().getId());
        values.put("date", incident.getDate());
        values.put("time", incident.getTime());
        values.put("victimSex", incident.getVictimSex());
        values.put("victimAge", incident.getVictimAge());
        values.put("incidentType", incident.getIncidentType());

        String args[] = {String.valueOf(incident.getId())};
        if (db.update("incident", values, "id=?", args) == 0) {
            db.insert("incident", null, values);
        }
    }

    public ArrayList<Incident> getAllIncidents() {
        ArrayList<Incident> incidents = new ArrayList<>();
        String columns[] = {"id", "userID", "addressID", "date", "time", "victimSex", "victimAge", "incidentType"};

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("incident", columns, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(0);
            Integer userID = null;
            if (!cursor.isNull(1)) {
                userID = cursor.getInt(1);
            }
            Integer addressID = null;
            if(!cursor.isNull(2)) {
                addressID = cursor.getInt(2);
            }
            String date = cursor.getString(3);
            String time = cursor.getString(4);
            String victimSex = cursor.getString(5);
            Integer victimAge = cursor.getInt(6);
            Integer incidentType = null;
            if(!cursor.isNull(7)) {
                incidentType = cursor.getInt(7);
            }

            Address address;
            if (addressID != null) {
                address = getAddressById(addressID);
            } else {
                return null;
            }

            Incident incident = new Incident(id, userID, address, date, time, victimSex, victimAge, incidentType);
            incidents.add(incident);
        }
        cursor.close();
        return incidents;

    }
}
