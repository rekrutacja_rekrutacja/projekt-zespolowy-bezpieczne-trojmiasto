package hu.pe.safe3city.safe3city;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.Toast;

import hu.pe.safe3city.safe3city.menu.fragments.LoginFragment;
import hu.pe.safe3city.safe3city.menu.fragments.NavigationDrawerFragment;

public class MainActivity extends ActionBarActivity {

    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private DrawerLayout drawerLayout;
    private boolean doubleBackToExitPressedOnce = false;
    private Integer backDoublePressTime = 2000;
    CheckConnection checkConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_appbar);
        toolbarInit();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        attachNavigationDrawer();
        checkConnection = new CheckConnection(this);
        fillDatabase();
    }

    private void fillDatabase() {
        APIConnect apiConnect = new APIConnect(this);
        apiConnect.fillDatabase();
    }

    private void toolbarInit() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void attachNavigationDrawer() {
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, drawerLayout, toolbar);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawer(Gravity.START);
            } else {
                drawerLayout.openDrawer(Gravity.START);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, backDoublePressTime);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        checkConnection.close();
        removeLoginData();
    }

    public void removeLoginData() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(LoginFragment.LOGIN_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().remove("userID").remove("accountType").apply();
    }
}