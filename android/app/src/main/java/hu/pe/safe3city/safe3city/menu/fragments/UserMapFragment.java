package hu.pe.safe3city.safe3city.menu.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import hu.pe.safe3city.safe3city.ApiDBHelper;
import hu.pe.safe3city.safe3city.Incident;
import hu.pe.safe3city.safe3city.R;

public class UserMapFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap map;
    private ArrayList<LatLng> markerPoints = new ArrayList<>();

    public static final String MODE_WALKING = "walking";
    public static final String MODE_BIKE = "bicycling";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.map_fragment, container, false);
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        try {
            MapsInitializer.initialize(getActivity().getApplication());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        getIncidentsFromDB();
        setLocationSettings();
    }

    private void getIncidentsFromDB() {
        ApiDBHelper dbHelper = new ApiDBHelper(getActivity());
        ArrayList<Incident> incidents = dbHelper.getAllIncidents();
        for (Incident i : incidents) {
            drawIncidentsMarkers(i);
        }
        dbHelper.close();
    }

    private void drawIncidentsMarkers(Incident incident) {
        String street = incident.getAddress().getStreet();
        String houseNumber = incident.getAddress().getHouseNumber();
        String date = incident.getDate();
        String time = incident.getTime();
        String[] types = getResources().getStringArray(R.array.incidents_types_array);
        String incidentTYpe = types[incident.getIncidentType() - 1];
        LatLng point = incident.getAddress().getLocation();

        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.street) + " " + noNull(street) + " " + noNull(houseNumber) + "\n");
        builder.append(getString(R.string.date) + noNull(date) + "\n");
        builder.append(getString(R.string.time) + noNull(time) + "\n");
        builder.append(getString(R.string.incident_type) + " " + incidentTYpe + "\n");

        String snippet = builder.toString();

        map.addCircle(new CircleOptions().center(point).fillColor(Color.argb(150, 250, 242, 12)).radius(80).strokeWidth(0));
        map.addMarker(new MarkerOptions().position(point).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker
                (BitmapDescriptorFactory
                .HUE_YELLOW)).draggable(false));

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.marker, null);

                TextView info = (TextView) v.findViewById(R.id.info);

                if (marker.getSnippet() != null) {
                    info.setText(marker.getSnippet());
                } else if (marker.getTitle() != null) {
                    info.setText(marker.getTitle());
                }
                return v;
            }
        });

    }

    private static String noNull(String nullable) {
        return nullable == null ? " " : nullable;
    }

    private void setLocationSettings() {
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}

