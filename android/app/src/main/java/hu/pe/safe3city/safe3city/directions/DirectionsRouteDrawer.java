package hu.pe.safe3city.safe3city.directions;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hu.pe.safe3city.safe3city.R;
import hu.pe.safe3city.safe3city.RoutingActivity;


// this class works ok, but can (and should!) be refactored to be more sophisticated, readable and cleaner
public class DirectionsRouteDrawer {

    public static final String NO_POINTS = "noPoints";
    private static final String OK = "ok";
    private String returnStatus = OK;
    private final GoogleMap map;
    private Activity parent = null;

    public DirectionsRouteDrawer(Activity parent, GoogleMap map) {
        this.parent = parent;
        this.map = map;
    }

    public String drawRoute(LatLng origin, LatLng dest, String routeMode) {

        String url = getDirectionsUrl(origin, dest, routeMode);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);
        return returnStatus;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest, String routeMode) {

        String start = "origin=" + origin.latitude + "," + origin.longitude;
        String end = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String mode = "mode=" + routeMode;
        String parameters = start + "&" + end + "&" + sensor + "&" + mode;
        String response = "json";

        return "https://maps.googleapis.com/maps/api/directions/" + response + "?" + parameters;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            inputStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                returnStatus = NO_POINTS;
                return;
            }

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {
                        distance = point.get("distance");
                        continue;
                    } else if (j == 1) {
                        duration = point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(4);
                lineOptions.color(Color.BLUE);
            }

            try {
                ((RoutingActivity) parent).getSupportActionBar().setTitle(parent.getString(R.string.distance_abr) + distance + ", " + parent.getString(R.string.time) + Integer.parseInt(duration) / 60 + parent.getString(R.string.minutes_abr));
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("DrawingRoute ", "Is drawing started from the RoutingActivity?");
            }
                ((RoutingActivity) parent).polyline(lineOptions);

                map.addPolyline(lineOptions);
        }
    }
}
