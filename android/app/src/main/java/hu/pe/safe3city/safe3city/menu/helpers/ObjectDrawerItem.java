package hu.pe.safe3city.safe3city.menu.helpers;

public class ObjectDrawerItem {

    public int icon;
    public String name;

    public ObjectDrawerItem(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}