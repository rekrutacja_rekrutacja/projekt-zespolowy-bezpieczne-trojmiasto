package hu.pe.safe3city.safe3city.directions;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import hu.pe.safe3city.safe3city.PlaceDetailsSetter;
import hu.pe.safe3city.safe3city.menu.fragments.ChooseRouteFragment;
import hu.pe.safe3city.safe3city.menu.helpers.PlaceDetailsJSONParser;


public class PlaceDetailsDownloader extends AsyncTask<String, Void, LatLng> {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_DETAILS = "/details";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyDd9FEw-E5ON8rVXvt2jzWp70NdGWJgqhY";

    private final PlaceDetailsSetter caller;
    private final int pointType;

    public PlaceDetailsDownloader(PlaceDetailsSetter caller, int pointType) {
        this.caller = caller;
        this.pointType = pointType;
    }

    @Override
    protected LatLng doInBackground(String... url) {
        LatLng placeLocation = null;
        try {
            String jsonData = getPlaceDetails(url[0]);
            JSONObject jObject = new JSONObject(jsonData);

            PlaceDetailsJSONParser parser = new PlaceDetailsJSONParser();
            placeLocation = parser.parse(jObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return placeLocation;
    }

    private String getPlaceDetails(String placeID) {

        HttpURLConnection connection = null;
        StringBuilder jsonResults = new StringBuilder();
        InputStreamReader in = null;
        try {
            URL url = new URL(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON + "?placeid=" + placeID + "&key=" + API_KEY);
            connection = (HttpURLConnection) url.openConnection();
            in = new InputStreamReader(connection.getInputStream(), "UTF-8");

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return jsonResults.toString();
    }

    @Override
    protected void onPostExecute(LatLng result) {
        if(ChooseRouteFragment.START == pointType) {
            caller.setStartLocation(result);
        } else if (ChooseRouteFragment.END == pointType) {
            caller.setEndLocation(result);
        } else {
            throw new IllegalArgumentException();
        }
        super.onPostExecute(result);
    }
}