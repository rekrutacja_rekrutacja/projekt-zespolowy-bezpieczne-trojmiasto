package hu.pe.safe3city.safe3city.menu.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hu.pe.safe3city.safe3city.R;

public class DrawerItemCustomAdapter extends ArrayAdapter<ObjectDrawerItem> {

    Context mContext;
    int layoutResourceId;
    ArrayList<ObjectDrawerItem> items;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, ArrayList<ObjectDrawerItem> items) {

        super(mContext, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.menu_item_icon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.menu_item_name);

        ObjectDrawerItem menuItem = items.get(position);

        imageViewIcon.setImageResource(menuItem.icon);
        textViewName.setText(menuItem.name);

        return listItem;
    }

}