package hu.pe.safe3city.safe3city.menu.fragments;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {


    private TimePickerDialog.OnTimeSetListener onTimeSet;

    public void setCallback(TimePickerDialog.OnTimeSetListener timeSetListener) {
        onTimeSet = timeSetListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog dialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSet, hour, minute, true);
        return dialog;
    }
}