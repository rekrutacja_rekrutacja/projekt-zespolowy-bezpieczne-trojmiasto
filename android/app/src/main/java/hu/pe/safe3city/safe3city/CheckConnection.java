package hu.pe.safe3city.safe3city;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckConnection {

    private final NetworkChangeReceiver receiver;
    private final Context parent;
    private boolean isConnected;
    private AlertDialog dialog;

    public CheckConnection(Context parent) {
        this.parent = parent;
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        parent.registerReceiver(receiver, filter);
    }

    public void close() {
        parent.unregisterReceiver(receiver);
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            isNetworkAvailable(context);
        }

        private void displayNotConnectedDialog() {
            if (dialog == null || !dialog.isShowing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(parent);
                builder.setTitle(parent.getString(R.string.no_internet))
                        .setMessage(parent.getString(R.string.connection_waiting))
                        .setCancelable(false);
                dialog = builder.create();
                dialog.show();
            }
        }

        private void isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {

                isConnected = checkNetworkState(connectivity);
            } else {
                isConnected = false;
            }
            if(!isConnected) {
                displayNotConnectedDialog();
            }
        }

        private Boolean checkNetworkState(ConnectivityManager connectivity) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        closeDialog();
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        private void closeDialog() {
            if (dialog != null && dialog.isShowing()) {
                dialog.cancel();
            }
        }
    }
}
