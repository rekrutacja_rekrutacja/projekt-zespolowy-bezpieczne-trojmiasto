package hu.pe.safe3city.safe3city;

import android.app.Activity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class APIConnect {

    private final Activity activity;

    public APIConnect(Activity activity) {
        this.activity = activity;
    }

    public void fillDatabase() {
        RequestQueue queue = Volley.newRequestQueue(activity);

        String url = "http://188.226.171.110/api/api.php/points";

        JsonObjectRequest incidentsRequest = new JsonObjectRequest
                (Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if ("OK".equals(status)) {
                                parseJSON(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(incidentsRequest);
    }

    private void parseJSON(JSONObject response) {
        ArrayList<Incident> incidentsFromAPI = new ArrayList<>();
        ArrayList<Address> addressesFromAPI = new ArrayList<>();
        try {
            JSONArray data = response.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsonIncident = data.getJSONObject(i);

                Integer id = (Integer) jsonIncident.get("id");
                Integer userID = JSONObject.NULL.equals(jsonIncident.get("userID")) ? null : (Integer) jsonIncident.getInt("userID");
                Integer incidentType = JSONObject.NULL.equals(jsonIncident.get("incidentType")) ? null : (Integer) jsonIncident.getJSONObject("incidentType").getInt("id");
                String date = jsonIncident.getString("date");
                String time = jsonIncident.getString("time");
                String victimSex = jsonIncident.getString("victimSex");
                Integer victimAge = (Integer) jsonIncident.get("victimAge");

                Address address = getAddress(data.getJSONObject(i).getJSONObject("address"));
                Incident incident = new Incident(id, userID, address, date, time, victimSex, victimAge, incidentType);

                addressesFromAPI.add(address);
                incidentsFromAPI.add(incident);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        updateDB(addressesFromAPI, incidentsFromAPI);
    }

    private Address getAddress(JSONObject address) {
        Address addressToReturn = null;
        try {
            Integer id = (Integer) address.get("id");
            String street = address.getString("street");
            String houseNumber = address.getString("houseNumber");
            String postalCode = address.getString("postalCode");
            String city = address.getString("city");
            Double latitude = address.getDouble("latitude");
            Double longitude = address.getDouble("longitude");

            addressToReturn = new Address(id, street, houseNumber, postalCode, city, latitude, longitude);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return addressToReturn;
    }

    private void insertIntoDB(Incident incident) {
        ApiDBHelper dbHelper = new ApiDBHelper(activity);

        dbHelper.addAddress(incident.getAddress());
        dbHelper.addIncident(incident);

        dbHelper.close();
    }

    private void updateDB(ArrayList<Address> addresses, ArrayList<Incident> incidents) {
        ApiDBHelper dbHelper = new ApiDBHelper(activity);

        dbHelper.updateAllAddresses(addresses);
        dbHelper.updateAllIncidents(incidents);

        dbHelper.close();
    }
}
