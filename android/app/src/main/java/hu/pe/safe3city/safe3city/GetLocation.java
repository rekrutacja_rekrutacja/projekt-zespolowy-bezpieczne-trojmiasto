package hu.pe.safe3city.safe3city;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GetLocation extends FragmentActivity implements View.OnClickListener,
        OnMapReadyCallback {

    public static final String LOCATION = "location";

    private MapView mapView;
    private GoogleMap map;
    private LatLng location;
    private CheckConnection checkConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);
        mapView = (MapView) findViewById(R.id.mapLocation);
        mapView.onCreate(savedInstanceState);
        checkConnection = new CheckConnection(this);
        mapView.getMapAsync(this);

        Button cancel = (Button) findViewById(R.id.cancelButton);
        Button ok = (Button) findViewById(R.id.okButton);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

    }

    private void setUpMap() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                drawMarkerAndSetLocation(point);
            }

            private void drawMarkerAndSetLocation(LatLng point) {
                map.clear();
                location = point;
                MarkerOptions options = new MarkerOptions();
                options.position(point);
                map.addMarker(options);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent data = new Intent();

        switch (id) {
            case R.id.cancelButton:
                location = null;
                data.putExtra(LOCATION, location);
                setResult(RESULT_CANCELED, data);
                finish();
                break;
            case R.id.okButton:
                data.putExtra(LOCATION, location);
                setResult(RESULT_OK, data);
                finish();
                break;
            default:
                break;
        }
    }

    //TODO: ? maybe it's not so good idea as I think?
    // now if user choose point on map and simply press BACK button location is saved (just like
    // when "OK' is tapped)
    @Override
    public void onBackPressed() {
        if (location != null) {
            Intent data = new Intent();
            data.putExtra(LOCATION, location);
            setResult(RESULT_OK, data);
            finish();
        }

        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setUpMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        checkConnection.close();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
