<?php include('_header.php'); ?>

<?php if ($login->passwordResetLinkIsValid() == true) { ?>
<form method="post" action="password_reset.php" name="new_password_form">
	<table class="password_reset">
		<tr>
			<td>
				<input type='hidden' name='name' value='<?php echo $_GET['name']; ?>' />
			</td>
			<td>
				<input type='hidden' name='password_reset_hash' value='<?php echo $_GET['verification_code']; ?>' />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password_new"><?php echo WORDING_NEW_PASSWORD; ?></label>
			</td>
			<td>
				<input id="password_new" type="password" name="password_new" pattern=".{6,}" required autocomplete="off" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password_repeat"><?php echo WORDING_NEW_PASSWORD_REPEAT; ?></label>
			</td>
			<td>
				<input id="password_repeat" type="password" name="password_repeat" pattern=".{6,}" required autocomplete="off" />
			</td>
		</tr>
	</table>
	<input type="submit" name="submit_new_password" value="<?php echo WORDING_SUBMIT_NEW_PASSWORD; ?>" />
</form>

<?php } else { ?>
<form method="post" action="password_reset.php" name="password_reset_form">
	<table class="password_reset">
		<tr class="center">
			<td>
				<label for="name"><?php echo WORDING_REQUEST_PASSWORD_RESET; ?></label>
			</td>
		</tr>
		<tr class="center">
			<td>
				<input id="name" type="text" name="name" required />
			</td>
		</tr>
	</table>
    <input type="submit" name="request_password_reset" value="<?php echo WORDING_RESET_PASSWORD; ?>" />
</form>
<?php } ?>

<a href="login.php"><?php echo WORDING_BACK_TO_LOGIN; ?></a>

<?php include('_footer.php'); ?>
