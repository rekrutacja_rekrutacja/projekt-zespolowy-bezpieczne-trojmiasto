<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Safe3city</title>
	<link rel="stylesheet" type="text/css" href="views/style.css" />
	<link rel="stylesheet" href="views/shake.css">
	
	<script src="views/map.js"></script>
	<script src="views/classie.js"></script>
	<script src="views/sidebar.js"></script>
	
	
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="views/map/jquery-1.9.1.min.js" type="text/javascript"></script>
	<?php 
		include('views/map/googlemap.php');
	?>
    <script src="views/map/jquery.tablednd.js" type="text/javascript"></script>
</head>
<body>
	<?php 
		include('sidebar.html');
	?>
	<div id="div">
	<?php
		if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1)
			echo '<div id="greetings">Witaj, '.$_SESSION['name'].'.</div>';
		else
			echo '<div id="greetings">
				<a id="bar-link" href="http://188.226.171.110/login.php?link=3"> Zaloguj się</a>, 
				aby uzyskać pełny dostęp do strony.</div>';	
	?>
	
	<div id="wrapper">
	<?php include('message.php'); ?>
		<?php include("views/menu.php"); ?>
		<div id="content">