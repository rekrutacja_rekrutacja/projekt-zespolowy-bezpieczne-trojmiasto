var center = null;
var map = null;
var center_of_map = new google.maps.LatLng(54.3480823, 18.6807574)

function initMap() {
 
	var myOptions = {
		center: center_of_map,
		zoom: 12,
		minZoom: 12,
	 
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: false,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
		navigationControl: true,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.LARGE
		}
	}
  
	map = new google.maps.Map(document.getElementById("googleMap"),myOptions);
	
	var allowedBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(54.296039, 18.425461), 
		new google.maps.LatLng(54.459356, 18.834709)
	);	  
  
	google.maps.event.addListener(map, 'drag', function() { checkBounds(); });
				
	function checkBounds() { 	
		if (allowedBounds) {
			var allowed_ne_lng = allowedBounds.getNorthEast().lng();
			var allowed_ne_lat = allowedBounds.getNorthEast().lat();
			var allowed_sw_lng = allowedBounds.getSouthWest().lng();
			var allowed_sw_lat = allowedBounds.getSouthWest().lat();

			var currentBounds = map.getBounds();
			var current_ne_lng = currentBounds.getNorthEast().lng();
			var current_ne_lat = currentBounds.getNorthEast().lat();
			var current_sw_lng = currentBounds.getSouthWest().lng();
			var current_sw_lat = currentBounds.getSouthWest().lat();

			var currentCenter = map.getCenter();
			var centerX = currentCenter.lng();
			var centerY = currentCenter.lat();

			if (current_ne_lng > allowed_ne_lng) centerX = centerX-(current_ne_lng-allowed_ne_lng);
			if (current_ne_lat > allowed_ne_lat) centerY = centerY-(current_ne_lat-allowed_ne_lat);
			if (current_sw_lng < allowed_sw_lng) centerX = centerX+(allowed_sw_lng-current_sw_lng);
			if (current_sw_lat < allowed_sw_lat) centerY = centerY+(allowed_sw_lat-current_sw_lat);

			map.setCenter(new google.maps.LatLng(centerY,centerX));
		}
	}
 
}
google.maps.event.addDomListener(window, 'load', initMap);
