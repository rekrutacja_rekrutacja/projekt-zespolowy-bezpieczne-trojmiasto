<?php 

require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');

?>
<script type="text/javascript">
var directionsService = new google.maps.DirectionsService();


var _mapPoints = new Array();


var _directionsRenderer = '';

var przesuwanie = '';

var autocomplete,autocompletex2;

var center = null;
var currentPopup;
var bounds = new google.maps.LatLngBounds();
var pt;



var option = {
	
	location: google.maps.LatLng(54.3480823, 18.6807574),
	radius: '500',
	country: 'pl',
	types: ['geocode'],

}


$(document).ready(function() {

	
	autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'),option)
	
	
	  
	autocompletex2 = new google.maps.places.Autocomplete(document.getElementById('autocompletex2'),option);
});




function calculateRoute(autocomplete, autocompletex2) {


    _directionsRenderer = new google.maps.DirectionsRenderer();

   
    var myOptions = {
		zoom: 12,
		minZoom:12,
        center: new google.maps.LatLng(54.3480823, 18.6807574),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	
 
    var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);

	
    <?php
	$db = new DAO();
	$address = new Address();
	$incident = new Incident();
	$incidentType = new IncidentType();
	
				$place = $db->address->getAll();
				$warrnings = $db->incident->getAll();
				$type = $db->incidentType->getAll();
				foreach ($place as $row) {
					$zatwierdzone = 0;
					$street=$row->street;
					$number = $row->houseNumber;
					$lat=$row->latitude;
					$lon=$row->longitude;
					
					foreach ($warrnings as $row) {
						if($row->address->street == $street && $row->address->houseNumber == $number )
						{
							if($row->userID == NULL)
							{
								
								$zatwierdzone = 1;
						
							}
							$date = $row->date;
							$time = $row->time;
							$typeID = $row->incidentType->name;
							
							
							foreach($type as $row)
							{
								if($row->name == $typeID)
								{
									$incident_name = $row->name;
									
									$incident_type_type = $row->type;
								}
							}
						}
					}
					
						
					if($zatwierdzone == 1)
					{
				
					?>
	
					var infowindow = new google.maps.InfoWindow({
						  content: "<?php echo($street); ?>"
					  });
					
					var marker = new google.maps.Marker({
						<?php echo("position: new google.maps.LatLng($lat, $lon),") ?>
						clickable: true
					
						
						});
						
						

						marker.setMap(map);
						
						
						 google.maps.event.addListener(marker, 'click', function() {
							infowindow.setContent("<?php echo("Ulica: " . $street . " " . $number . "</br> Data wydarzenia: " . $date . "</br>Godzina: " . $time . " </br>Incydent: " . $incident_name); ?>");
							infowindow.open(map, this);

						  });
					
				$zdarzenie = "<?php echo($incident_type_type); ?>";
			
				if($zdarzenie  == "Człowiek")
				{				
						var myCity = new google.maps.Circle({
						<?php	  echo("center: new google.maps.LatLng($lat, $lon),") ?>
							radius:100,
							strokeColor:"red",
							strokeOpacity:0.2,
							strokeWeight:2,
							fillColor:"red",
							fillOpacity:0.4
					});

							myCity.setMap(map);
					
				}else{
					
					var myCity = new google.maps.Circle({
						<?php	  echo("center: new google.maps.LatLng($lat, $lon),") ?>
							radius:100,
							strokeColor:"#FF9900",
							strokeOpacity:0.2,
							strokeWeight:2,
							fillColor:"#FF9900",
							fillOpacity:0.4
					});

							myCity.setMap(map);
					
				}
				
			<?php
					}
				}
				
			?>
	
	
  
    _directionsRenderer.setMap(map);
    

    _directionsRenderer.setOptions({
        draggable: true
    });
	
	
    

	
	image = 'views/images/a.png';
   
    google.maps.event.addListener(map, "click", function (event) {
			if(!(autocomplete && autocompletex2)) {
				
				_mapPoints.push(event.latLng);
				if(_mapPoints.length == 1)
				{
					marker = new google.maps.Marker({
						position: _mapPoints[0],
						icon : image
					});

					marker.setMap(map);
					
				}
				else {
					marker.setMap(null);
					getRoutePointsAndWaypoints();
				
				}
			}
		
    });
	

    
	if(autocomplete && autocompletex2)
	{
		
		var skad = autocomplete + ", Gdańsk";
		var dokad = autocompletex2 + ", Gdańsk";
		 _directionsRenderer.setMap(null);
		 _directionsRenderer.setOptions({
			draggable: false
		});	
	} 
	
	 var directionsRequest = {
		  
          origin: skad,
          destination: dokad,
          travelMode: google.maps.DirectionsTravelMode.WALKING,
          unitSystem: google.maps.UnitSystem.METRIC
        };
		
        directionsService.route(
          directionsRequest,
		 
          function(response, status)
          {
          
            przesuwanie =  new google.maps.DirectionsRenderer({
                map: map,
                directions: response,

              });
			  
			   przesuwanie.setOptions({
				draggable: true
			 });	
            
            	directionsService.route(directionsRequest, function (_response, _status) {
					if (_status == google.maps.DirectionsStatus.OK) {
						_directionsRenderer.setDirections(_response);
					}
				});

			
			 google.maps.event.addListener(przesuwanie, 'directions_changed', function () {
				computeTotalDistanceforRoute(przesuwanie.directions);
			});
          }
        );
		
		
		
	
	
	 google.maps.event.addListener(_directionsRenderer, 'directions_changed', function () {
        computeTotalDistanceforRoute(_directionsRenderer.directions);
    });
	
	var allowedBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(54.296039, 18.425461), 
		new google.maps.LatLng(54.459356, 18.834709)
	);	  
  
	google.maps.event.addListener(map, 'drag', function() { checkBounds(); });
				
	function checkBounds() { 	
		if (allowedBounds) {
			var allowed_ne_lng = allowedBounds.getNorthEast().lng();
			var allowed_ne_lat = allowedBounds.getNorthEast().lat();
			var allowed_sw_lng = allowedBounds.getSouthWest().lng();
			var allowed_sw_lat = allowedBounds.getSouthWest().lat();
			var currentBounds = map.getBounds();
			var current_ne_lng = currentBounds.getNorthEast().lng();
			var current_ne_lat = currentBounds.getNorthEast().lat();
			var current_sw_lng = currentBounds.getSouthWest().lng();
			var current_sw_lat = currentBounds.getSouthWest().lat();
			var currentCenter = map.getCenter();
			var centerX = currentCenter.lng();
			var centerY = currentCenter.lat();
			if (current_ne_lng > allowed_ne_lng) centerX = centerX-(current_ne_lng-allowed_ne_lng);
			if (current_ne_lat > allowed_ne_lat) centerY = centerY-(current_ne_lat-allowed_ne_lat);
			if (current_sw_lng < allowed_sw_lng) centerX = centerX+(allowed_sw_lng-current_sw_lng);
			if (current_sw_lat < allowed_sw_lat) centerY = centerY+(allowed_sw_lat-current_sw_lat);
			map.setCenter(new google.maps.LatLng(centerY,centerX));
		}
	}		
}



		$(document).ready(function() {
        // If the browser supports the Geolocation API
        if (typeof navigator.geolocation == "undefined") {
          $("#error").text("Your browser doesn't support the Geolocation API");
          return;
        }
 
        $("#from-link, #to-link").click(function(event) {
          event.preventDefault();
          var addressId = this.id.substring(0, this.id.indexOf("-"));
 
          navigator.geolocation.getCurrentPosition(function(position) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
              "location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
            },
            function(results, status) {
              if (status == google.maps.GeocoderStatus.OK)
                $("#" + addressId).val(results[0].formatted_address);
              else
                $("#error").append("Unable to retrieve your address<br />");
            });
          },
          function(positionError){
            $("#error").append("Error: " + positionError.message + "<br />");
          },
          {
            enableHighAccuracy: true,
            timeout: 10 * 1000 // 10 seconds
          });
        });
 
        $("#calculate-route").submit(function(event) {
          event.preventDefault();
          calculateRoute($("#autocomplete").val(), $("#autocompletex2").val());
        });
      });


function getRoutePointsAndWaypoints() {
    
    var _waypoints = new Array();
	
	
    if (_mapPoints.length > 1) 
    {
        for (var j = 1; j < _mapPoints.length - 1; j++) {
            var address = _mapPoints[j];
            if (address !== "") {
                _waypoints.push({
                    location: address,
                     stopover : true
                });
            }
        }
        
        drawRoute(_mapPoints[0], _mapPoints[_mapPoints.length - 1], _waypoints);

    } 
}


function drawRoute(originAddress, destinationAddress, _waypoints) {

    var _request = '';

        _request = {
            origin: originAddress,
            destination: destinationAddress,
			optimizeWaypoints: true,
			
            travelMode: google.maps.DirectionsTravelMode.WALKING,
			unitSystem: google.maps.UnitSystem.METRIC
			
        };

    directionsService.route(_request, function (_response, _status) {
        if (_status == google.maps.DirectionsStatus.OK) {
            _directionsRenderer.setDirections(_response);
        }
    });
}



var _htmlTrCount = 0;


function computeTotalDistanceforRoute(_result) {
   
    var _route = _result.routes[0];

  
    $("#HtmlTable").find("tr").remove();

  
    var _temPoint = new Array();

    _htmlTrCount = 0;
    for (var k = 0; k < _route.legs.length; k++) {
    
        var lenght = 0;
        if ((_route.legs[k].steps.length) - 1 < 0) {
            var lenght = _route.legs[k].steps.length;
        } else {
            var lenght = _route.legs[k].steps.length - 1;
        }


        if (_route.legs[k].distance.value > 0) 
        {
            if (k == 0) 
            {
                _temPoint.push(_route.legs[k].steps[0].start_point); 
                _htmlTrCount++;
                
                _temPoint.push(_route.legs[k].steps[lenght].end_point); 
                _htmlTrCount++;
                CreateHTMTable(_route.legs[k].steps[lenght].end_point, _route.legs[k].distance.value); 
            } 
        } else 
        {
            _temPoint.push(_route.legs[k].steps[lenght].start_point); 
            _htmlTrCount++;
            CreateHTMTable(_route.legs[k].steps[lenght].start_point, _route.legs[k].distance.value); 
        }
    }

    
    _mapPoints = new Array();
    for (var y = 0; y < _temPoint.length; y++) {
        _mapPoints.push(_temPoint[y]);
		
    }
	
}



function CreateHTMTable(_latlng, _distance) {
    var _Speed = 5;
	if(_distance > 100)
	{	
		odleglosc = _distance/1000;
		odleglosc = odleglosc.toFixed(2);
	}
    var _Time = parseInt(((_distance / 1000) / _Speed) * 60);
	

	
    if (_htmlTrCount - 1 == 0) {
        _Time = 0;
        _distance = 0;
    }
	
	
	var html = '';
    html = html + "<tr id=\"" + _htmlTrCount + "\">";
	html = html + "<td>Dystans</td>";
	if(_distance < 100) {
    html = html + "<td style=\"width: 100px;\"><span id=\"dir_" + _htmlTrCount + "\">" + _distance + " m</span></td>"; }
	if(_distance >= 100) {
    html = html + "<td style=\"width: 100px;\"><span id=\"dir_" + _htmlTrCount + "\">" + odleglosc + " km </span></td>"; }
	html = html + "</tr>";
	html = html + "<tr>";
	html = html + "<td>Czas</td>";
    html = html + "<td style=\"width: 70px;\"><span id=\"time_" + _htmlTrCount + "\">" + _Time + " min</span></td>";
	html = html + "</tr>";
	html = html + "<tr>";
	html = html + "<td>Usuń trasę</td>";
    html = html + "<td style=\"width: 60px;\"><img alt=\"DeleteLocation\" src=\"views/images/delete.jpg\" onclick=\"return deleteLocation(" + _htmlTrCount + ");\" /></td>";
    html = html + "</tr>";
    $("#HtmlTable").append(html);
    draganddrophtmltablerows();
}


function deleteLocation(trid) {
    if (confirm("Are you sure want to delete this location?") == true) {
        var _temPoint = new Array();
        for (var w = 0; w < _mapPoints.length; w++) {
            if (trid != w + 1) {
                _temPoint.push(_mapPoints[w]);
            }
        }

        _mapPoints = new Array();
        for (var y = 0; y < _temPoint.length; y++) {
			
				_mapPoints.push(_temPoint[y]);
				
        }
	
		
        location.reload();
		
		
    } else {
        return false;
    }
}
	

function draganddrophtmltablerows() {
    var _tempPoints = new Array();

   
    $("#HtmlTable").tableDnD();

   
    $("#HtmlTable").tableDnD({
        onDrop: function (table, row) {
            var rows = table.tBodies[0].rows;

            for (var q = 0; q < rows.length; q++) {
                _tempPoints.push(_mapPoints[rows[q].id - 1]);
            }

            _mapPoints = new Array();
            for (var y = 0; y < _tempPoints.length; y++) {
                _mapPoints.push(_tempPoints[y]);
            }

            getRoutePointsAndWaypoints();
        }
    });
}

google.maps.event.addDomListener(window, 'load', calculateRoute);
</script>