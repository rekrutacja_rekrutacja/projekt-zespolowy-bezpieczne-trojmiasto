<?php include('_header.php'); ?>

<form method="post" action="login.php" name="loginform">
	<table class="login">
		<tr>
			<td>
				<label for="name"><?php echo WORDING_USERNAME; ?></label>
			</td>
			<td>
				<input id="name" type="text" name="name" required />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password"><?php echo WORDING_PASSWORD; ?></label>
			</td>
			<td>
				<input id="password" type="password" name="password" autocomplete="off" required />
			</td>
		</tr>
		<tr>
			<td>
				<label for="rememberme"><?php echo WORDING_REMEMBER_ME; ?></label>
			</td>
			<td class="center">
				<input type="checkbox" id="rememberme" name="rememberme" value="1" />
			</td>
		</tr>
	</table>
	<input class="form-button" type="submit" name="login" value="<?php echo WORDING_LOGIN; ?>" />
	
</form>

<table id="forgot-password">
	<tr>
		<td>
			<a href="register.php"><?php echo WORDING_REGISTER_NEW_ACCOUNT; ?></a>
		</td>
	<td>
			<a href="password_reset.php"><?php echo WORDING_FORGOT_MY_PASSWORD; ?></a>
		</td>
</tr>
</table>

<?php include('_footer.php'); ?>
