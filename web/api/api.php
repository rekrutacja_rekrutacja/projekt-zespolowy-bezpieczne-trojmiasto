<?php

	$GLOBALS['f3'] = require_once ($_SERVER['DOCUMENT_ROOT'].'/libraries/f3/base.php');
	
	$f3->map('/api.php/login','Login');
	$f3->map('/api.php/point/@params', 'Point');
	$f3->map('/api.php/points', 'Points');
	$f3->map('/api.php/pointsBy/@params', 'PointsBy');
	$f3->map('/api.php/add', 'Add');
	
	$f3->run();

	
	class Login {
	
		function post() {

			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Account.php');

			$decodedPost = json_decode(file_get_contents('php://input'), true);
			$login = $decodedPost['login'];
			$password = $decodedPost['password'];

			$db = new DAO();
			
			$account = null;
			
			if(is_null($account)) {
				$account = $db->user->findByLogin($login);
			}
			
			if(is_null($account)) {
				$account = $db->moderator->findByLogin($login);
			}
			
			if(is_null($account)) {
				$account = $db->admin->findByLogin($login);
			}
			
			if(!is_null($account)) {
			
				if (password_verify($password, $account->passwordHash)) {
					
					$account = $db->login->tryLogIn($login, $account->passwordHash);
			
					if(!is_null($account)) {
					
						header('Content-Type: application/json');
						echo json_encode(array('status' => 'OK', 'data' => 
							array('id' => $account->accountObject->id,
								'accountType' => $account->accountType)));
					}
				}
				else {
					header('Content-Type: application/json');
					echo json_encode(array('status' => 'NOT_FOUND'));
				}
			}
			else {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'NOT_FOUND'));
			}
		}
	}
	
	class Point {
	
		function get($params) {
		
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
			
			$db = new DAO();
			$id = $params['REQUEST']['id'];
			$point = $db->incident->findByID((int)$id);

			if(!is_null($point)) {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'OK', 'data' => $point));
			}
			else {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'NOT_FOUND'));
			}
		}
	}
	
	class Points {
	
		function get() {
			
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
			
			$db = new DAO();
			$points = $db->incident->getAll();
			
			if(!is_null($points)) {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'OK', 'data' => $points));
			}
			else {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'ZERO_RESULTS'));
			}
		}
	}
	
	class PointsBy {
	
		function get($params) {
			
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
			
			$db = new DAO();
			if(key($params['REQUEST']) === 'incidentName')
				$points = $db->incident->findByIncidentName($params['REQUEST']['incidentName']);
			elseif(key($params['REQUEST']) === 'incidentType')
				$points = $db->incident->findByIncidentType($params['REQUEST']['incidentType']);
			else
				$points = null;
				
			if(!is_null($points)) {
				http_response_code(200);
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'OK', 'data' => $points));
			}
			else {
				header('Content-Type: application/json');
				echo json_encode(array('status' => 'NOT_FOUND'));
			}
		}
	}
	
	class Add {
		
		function post() {
			
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
			require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
			
			$address = new Address();
			
			$decodedPost = json_decode(file_get_contents('php://input'), true);

			$address->street = $decodedPost['street'];
			$address->houseNumber = $decodedPost['houseNumber'];
			$address->postalCode = $decodedPost['postalCode'];
			$address->city = $decodedPost['city'];
			$address->latitude = $decodedPost['latitude'];
			$address->longitude = $decodedPost['longitude'];
			
			
			$incident = new Incident();
			
			$incident->userID = $decodedPost['userID'];
			$incident->address = $address;
			$incident->incidentType->id = $decodedPost['incidentTypeID'];
			$incident->date = $decodedPost['date'];
			$incident->time = $decodedPost['time'];
			$incident->victimSex = $decodedPost['victimSex'];
			$incident->victimAge = $decodedPost['victimAge'];
			
			$db = new DAO();
			$db->incident->add($incident);
			
			header('Content-Type: application/json');
			echo json_encode(array('status' => 'CREATED'));
		}
	}
?>