<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/User.php');
	require_once ('randomString.php');
	
	
	$db = new DAO();
		
		
	echo 'ADD USER TEST:<br>';
	$user = new User();
	$user->login = randomString();
	$user->passwordHash = randomString();
	$user->email = randomString();
	$db->user->add($user);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND USER TESTS:<br>';
	echo '-> by ID (99):<br>';
	$user = $db->user->findByID(99);
	if(is_null($user))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
	}
	
	echo '<br><br>-> by Login (fX5mHGD7UW):<br>';
	$user = $db->user->findByLogin('fX5mHGD7UW');
	if(is_null($user))
		echo 'NOT FOUND';
	else
	{
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL USERS TEST:<br>';
	$all_users = $db->user->getAll();
	if(is_null($all_users))
		echo 'NOTHING FOUND';
	else {
		while (list(, $user) = each($all_users)) {
			while (list($key, $value) = each($user)) {
				echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';


	echo 'DELETE USER TESTS:<br>';
	echo '-> by ID (138) + try to find:<br>';
	$db->user->deleteByID(138);
	$user = $db->user->findByID(138);
	if(is_null($user))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	
	echo '<br><br>-> by Login (a56bu0lQSP) + try to find:<br>';
	$db->user->deleteByLogin('a56bu0lQSP');
	$user = $db->user->findByLogin('a56bu0lQSP');
	if(is_null($user))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	
	
	/*
	echo 'DELETE ALL USERS TEST + try get all users:<br>';
	$db->user->deleteAll();

	$all_users = $db->user->getAll();
	if(is_null($all_users))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	*/
	
	
	echo 'UPDATE USER TEST:<br>';
	echo '-> before (ID 167):<br>';
	$user = $db->user->findByID(167);
	if(is_null($user))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
	
		$user->login = 'abc';
		$user->passwordHash = 'abc';
		$user->email = 'abc';
		$user->isActive = 1;
		$user->activationHash = 'abc';
		$user->passwordResetHash = 'abc';
		$user->passwordResetTimestamp = 123;
		$user->rememberMeToken = 'abc';
		$user->failedLogins = 123;
		$user->lastFailedLogin = '123';
		$user->registrationDatetime = '2001-01-01 12:34:56';
		
		$db->user->update($user);
		
		echo '<br><br>-> after (ID 167):<br>';
		$user = $db->user->findByID(167);
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'EXTRA USER FUNCTIONS TEST (dependent on the previous):<br>';
	$user = $db->user->findByID(167);
	if(is_null($user))
		echo 'NOT FOUND';
	else {
		$db->user->changeLogin($user, 'new login');
		$db->user->changeEmail($user, 'new email');
		$db->user->deactivate($user);
		$user = $db->user->findByID(167);
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
		
		$db->user->activate($user);
		$user = $db->user->findByID(167);
		while (list($key, $value) = each($user)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';


	echo '<br><br>DONE';
?>