<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
	require_once ('randomString.php');

	
	$db = new DAO();
		
		
	echo 'ADD INCIDENT TYPE TEST:<br>';
	$incidentType = new IncidentType();
	$incidentType->name = randomString();
	$incidentType->type = randomString();
	$incidentType->weight = 123;
	
	$db->incidentType->add($incidentType);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND INCIDENT TYPE TESTS:<br>';
	echo '-> by ID (1):<br>';
	$incidentType = $db->incidentType->findByID(1);
	if(is_null($incidentType))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($incidentType)) {
			echo "$key: $value<br>";
		}
	}
	
	echo '<br><br>-> by Name (Urwane lusterko):<br>';
	$incidentType = $db->incidentType->findByName('Urwane lusterko');
	if(is_null($incidentType))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($incidentType)) {
			echo "$key: $value<br>";
		}
	}
	
	echo '<br><br>-> by Type (Mienie):<br>';
	$incidentTypes = $db->incidentType->findByType('Mienie');
	if(is_null($incidentType))
		echo 'NOTHING FOUND';
	else {
		while (list(, $incidentType) = each($incidentTypes)) {
			while (list($key, $value) = each($incidentType)) {
				echo "$key: $value<br>";
			}
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL INCIDENT TYPES TEST:<br>';
	$all_incidentTypes = $db->incidentType->getAll();
	if(is_null($all_incidentTypes))
		echo 'NOTHING FOUND';
	else {
		while (list(, $incidentType) = each($all_incidentTypes)) {
			while (list($key, $value) = each($incidentType)) {
				echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'DELETE INCIDENT TYPE TEST:<br>';
	echo '-> by ID (23) + try to find:<br>';
	$db->incidentType->deleteByID(23);
	$incidentType = $db->incidentType->findByID(23);
	if(is_null($incidentType))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
		
	echo '<br><br>-> by Name (Morderstwo) + try to find:<br>';
	$db->incidentType->deleteByName('Morderstwo');
	$incidentType = $db->incidentType->findByName('Morderstwo');
	if(is_null($incidentType))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	
	
	/*
	echo 'DELETE ALL INCIDENT TYPES TEST + try get all incident types:<br>';
	$db->incidentType->deleteAll();

	$all_incidentTypes = $db->incidentType->getAll();
	if(is_null($all_incidentTypes))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	*/
	
	
	echo 'UPDATE INCIDENT TYPE TEST:<br>';
	echo '-> before (ID 26):<br>';
	$incidentType = $db->incidentType->findByID(26);
	if(is_null($incidentType))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($incidentType)) {
			echo "$key: $value<br>";
		}
		
		$incidentType->name = 'new name';
		$incidentType->type = 'new type';
		$incidentType->weight = 0;

		$db->incidentType->update($incidentType);
		
		echo '<br>-> after (ID 26):<br>';
		$incidentType = $db->incidentType->findByID(26);
		while (list($key, $value) = each($incidentType)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	echo '<br><br>DONE';
?>