<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
	require_once ('randomString.php');

	
	$db = new DAO();
		
		
	echo 'ADD ADDRESS TEST:<br>';
	$address = new Address();
	$address->street = randomString();
	$address->houseNumber = randomString();
	$address->postalCode = '12-345';
	$address->city = randomString();
	$address->latitude = 12.345;
	$address->longitude = 12.345;

	$db->address->add($address);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND ADDRESS TEST:<br>';
	echo '-> by ID (1):<br>';
	$address = $db->address->findByID(1);
	if(is_null($address))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($address)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL ADDRESSES TEST:<br>';
	$all_addresses = $db->address->getAll();
	if(is_null($all_addresses))
		echo 'NOTHING FOUND';
	else {
		while (list(, $address) = each($all_addresses)) {
			while (list($key, $value) = each($address)) {
				echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'DELETE ADDRESS TEST:<br>';
	echo '-> by ID (4) + try to find:<br>';
	$db->address->deleteByID(4);
	$address = $db->address->findByID(4);
	if(is_null($address))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';

	
	/*
	echo 'DELETE ALL ADDRESSES TEST + try get all addresses:<br>';
	$db->address->deleteAll();

	$all_addresses = $db->address->getAll();
	if(is_null($all_addresses))
		echo 'SUCCESS';
	else
		echo 'FAILURE!'
	echo '<br>------------------------------------<br><br>';
	*/
	
	echo 'UPDATE ADDRESS TEST:<br>';
	echo '-> before (ID 9):<br>';
	$address = $db->address->findByID(9);
	if(is_null($address))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($address)) {
			echo "$key: $value<br>";
		}
		
		$address->street = 'abc';
		$address->houseNumber = 123;
		$address->postalCode ='abc';
		$address->city = 'abc';
		$address->latitude = 0;
		$address->longitude = 0;
		
		$db->address->update($address);
		
		echo '<br><br>-> after (ID 9):<br>';
		$address = $db->address->findByID(9);
		while (list($key, $value) = each($address)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	echo '<br><br>DONE';
?>