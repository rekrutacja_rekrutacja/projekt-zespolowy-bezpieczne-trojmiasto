<?php
	class IncidentManager {
	
		private $db;
		private $addressManager;
		private $incidentTypeManager;
		
		private function mapDBincidentToIncident($DBIncident) {
		
			$incident = new Incident();
			
			$incident->id = $DBIncident->id;
			$incident->userID = $DBIncident->user_id;
			$incident->address = $this->addressManager->findByID($DBIncident->address_id);
			$incident->incidentType = $this->incidentTypeManager->findByID($DBIncident->incident_type_id);
			$incident->date = $DBIncident->date;
			$incident->time = $DBIncident->time;
			$incident->victimSex = $DBIncident->victim_sex;
			$incident->victimAge = $DBIncident->victim_age;
			
			return $incident;
		}
		
		private function fillDBIncidentWithData($DBIncident, $modelIncident) {
		
			$DBIncident->user_id = $modelIncident->userID;
		
			if(is_null($modelIncident->address->id)) {
				$id = $this->addressManager->add($modelIncident->address);
				$DBIncident->address_id = $id;
			}
			else {
				$this->addressManager->update($modelIncident->address);
				$DBIncident->address_id = $modelIncident->address->id;
			}
			
			if(is_null($modelIncident->incidentType->id)) {
				$id = $this->incidentTypeManager->add($modelIncident->incidentType);
				$DBIncident->incident_type_id = $id;
			}
			else {
				$this->incidentTypeManager->update($modelIncident->incidentType);
				$DBIncident->incident_type_id = $modelIncident->incidentType->id;
			}
		
			$DBIncident->date = $modelIncident->date;
			$DBIncident->time = $modelIncident->time;
			$DBIncident->victim_sex = $modelIncident->victimSex;
			$DBIncident->victim_age = $modelIncident->victimAge;
		
			return $DBIncident;
		}
		
		
		public function __construct($connection, $addressManager, $incidentTypeManager)
		{
			$this->db = $connection;
			$this->addressManager = $addressManager;
			$this->incidentTypeManager = $incidentTypeManager;
		}
		
		
		public function add($newIncident) {
		
			$incident = new DB\SQL\Mapper($this->db,'incident');
			
			$incident->user_id = $newIncident->userID;
			
			if(is_null($newIncident->address->id)) {
				$id = $this->addressManager->add($newIncident->address);
				$incident->address_id = $id;
			}
			else $incident->address_id = $newIncident->address->id;
			
			if(is_null($newIncident->incidentType->id)) {
				$id = $this->incidentTypeManager->add($newIncident->incidentType);
				$incident->incident_type_id = $id;
			}
			else $incident->incident_type_id = $newIncident->incidentType->id;
		
			$incident->date = $newIncident->date;
			$incident->time = $newIncident->time;
			$incident->victim_sex = $newIncident->victimSex;
			$incident->victim_age = $newIncident->victimAge;

			$incident->save();
			return $incident->id;
		}
		
		public function findByID($id) {
		
			$retrievedIncident = new DB\SQL\Mapper($this->db, 'incident');
			$retrievedIncident->load(array('id=?', $id));
			
			if(is_null($retrievedIncident->id))
				return null;
			
			return $this->mapDBincidentToIncident($retrievedIncident);
		}
		
		public function findByIncidentName($name) {
		
			$incidentType = $this->incidentTypeManager->findByName($name);
			
			if(is_null($incidentType))
				return null;
				
			$id = $incidentType->id;
			
			$retrievedIncident = new DB\SQL\Mapper($this->db, 'incident');
			$retrievedIncident->load(array('incident_type_id=?', $id));
			
			if(is_null($retrievedIncident->id))
				return null;
				
			$incidents = array();
			
			do { 
				if(is_null($retrievedIncident->user_id))
					array_push($incidents, $this->mapDBincidentToIncident($retrievedIncident)); 
				
			} while($retrievedIncident->next());
			
			return sizeof($incidents)>0 ? $incidents : null;
		}
		
		public function findByIncidentType($type) {
		
			$incidents = $this->incidentTypeManager->findByType($type);
			
			return sizeof($incidents)>0 ? $incidents : null;
		}
		
		public function getAll() {
		
			$retrievedIncident = new DB\SQL\Mapper($this->db, 'incident');
			$retrievedIncident->load();
			
			if(is_null($retrievedIncident->id))
				return null;
				
			$incidents = array();
			
			do { 
				if(is_null($retrievedIncident->user_id))
					array_push($incidents, $this->mapDBincidentToIncident($retrievedIncident)); 
				
			} while($retrievedIncident->next());
			
			return sizeof($incidents)>0 ? $incidents : null;
		}
		
		public function getUnapproved() {
		
			$retrievedIncident = new DB\SQL\Mapper($this->db, 'incident');
			$retrievedIncident->load();
			
			if(is_null($retrievedIncident->id))
				return null;
				
			$unapprovedIncidents = array();
			
			do { 
				if(!is_null($retrievedIncident->user_id))
					array_push($unapprovedIncidents, $this->mapDBincidentToIncident($retrievedIncident)); 
				
			} while($retrievedIncident->next());
			
			return sizeof($unapprovedIncidents)>0 ? $unapprovedIncidents : null;
		}
		
		public function deleteByID($id) {
		
			$incidentToDelete = new DB\SQL\Mapper($this->db, 'incident');
			$incidentToDelete->load(array('id=?', $id));
			
			$incidentToDelete->erase();
		}
		
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM incident');
		}
		
		public function update($incident) {

			$incidentToUpdate = new DB\SQL\Mapper($this->db, 'incident');
			$incidentToUpdate->load(array('id=?', $incident->id));
			$incidentToUpdate = $this->fillDBIncidentWithData($incidentToUpdate, $incident);
		
			$incidentToUpdate->save();
		}
		
		public function accept($incident) {
		
			$incidentToAccept = new DB\SQL\Mapper($this->db, 'incident');
			$incidentToAccept->load(array('id=?', $incident->id));
			
			$incidentToAccept->user_id = null;
			$incidentToAccept->save();
		}
	}
?>