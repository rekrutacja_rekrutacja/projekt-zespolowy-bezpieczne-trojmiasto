<?php
	class UserManager {
	
		private $db;
		
		private function mapDBUserToUser($DBUser) {
		
			$user = new User();
			
			$user->id = $DBUser->user_id;
			$user->login = $DBUser->user_name;
			$user->passwordHash = $DBUser->user_password_hash;
			$user->email = $DBUser->user_email;
			$user->isActive = $DBUser->user_active;
			$user->activationHash = $DBUser->user_activation_hash;
			$user->passwordResetHash = $DBUser->user_password_reset_hash;
			$user->passwordResetTimestamp = $DBUser->user_password_reset_timestamp;
			$user->rememberMeToken = $DBUser->user_rememberme_token;
			$user->failedLogins = $DBUser->user_failed_logins;
			$user->lastFailedLogin = $DBUser->user_last_failed_login;
			$user->registrationDatetime = $DBUser->user_registration_datetime;
			
			return $user;
		}
		
		private function fillDBUserWithData($DBUser, $modelUser) {
		
			$DBUser->user_name = $modelUser->login;
			$DBUser->user_password_hash = $modelUser->passwordHash;
			$DBUser->user_email = $modelUser->email;
			$DBUser->user_active = $modelUser->isActive;
			$DBUser->user_activation_hash = $modelUser->activationHash;
			$DBUser->user_password_reset_hash = $modelUser->passwordResetHash;
			$DBUser->user_password_reset_timestamp = $modelUser->passwordResetTimestamp;
			$DBUser->user_rememberme_token = $modelUser->rememberMeToken;
			$DBUser->user_failed_logins = $modelUser->failedLogins;
			$DBUser->user_last_failed_login = $modelUser->lastFailedLogin;
			$DBUser->user_registration_datetime = $modelUser->registrationDatetime;
			
			return $DBUser;
		}
		
		
		public function __construct($connection)
		{
			$this->db = $connection;
		}
		
		
		public function add($newUser) {
		
			$user = new DB\SQL\Mapper($this->db,'users');
			
			$user->user_name = $newUser->login;
			$user->user_password_hash = $newUser->passwordHash;
			$user->user_email = $newUser->email;
			$user->user_active = $newUser->isActive;
			$user->user_activation_hash = $newUser->activationHash;
			$user->user_password_reset_hash = $newUser->passwordResetHash;
			$user->user_password_reset_timestamp = $newUser->passwordResetTimestamp;
			$user->user_rememberme_token = $newUser->rememberMeToken;
			$user->user_failed_logins = $newUser->failedLogins;
			$user->user_last_failed_login = $newUser->lastFailedLogin;
			$user->user_registration_datetime = $newUser->registrationDatetime;
			
			$user->save();
			return $user->user_id;
		}
		
		public function findByID($id) {
		
			$retrievedUser = new DB\SQL\Mapper($this->db, 'users');
			$retrievedUser->load(array('user_id=?', $id));
			
			if(is_null($retrievedUser->user_id))
				return null;
			
			return $this->mapDBUserToUser($retrievedUser);
		}
		
		public function findByLogin($login) {
		
			$retrievedUser = new DB\SQL\Mapper($this->db, 'users');
			$retrievedUser->load(array('user_name=?', $login));
			
			if(is_null($retrievedUser->user_id))
				return null;
		
			return $this->mapDBUserToUser($retrievedUser);
		}
		
		public function findByEmail($email) {
		
			$retrievedUser = new DB\SQL\Mapper($this->db, 'users');
			$retrievedUser->load(array('user_email=?', $email));
			
			if(is_null($retrievedUser->user_id))
				return null;
		
			return $this->mapDBUserToUser($retrievedUser);
		}
		
		public function getAll() {
		
			$retrievedUser = new DB\SQL\Mapper($this->db, 'users');
			$retrievedUser->load();
			
			if(is_null($retrievedUser->user_id))
				return null;
			
			$users = array();
			
			do { 
				array_push($users, $this->mapDBUserToUser($retrievedUser)); 
				
			} while($retrievedUser->next());
			
			return $users;
		}
		
		public function deleteByID($id) {
		
			$userToDelete = new DB\SQL\Mapper($this->db, 'users');
			$userToDelete->load(array('user_id=?', $id));
			
			$userToDelete->erase();
		}
		
		public function deleteByLogin($login) {
		
			$userToDelete = new DB\SQL\Mapper($this->db, 'users');
			$userToDelete->load(array('user_name=?', $login));
			
			$userToDelete->erase();
		}
	
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM users');
		}
		
		public function update($user) {
		
			$userToUpdate = new DB\SQL\Mapper($this->db, 'users');
			$userToUpdate->load(array('user_id=?', $user->id));
			$userToUpdate = $this->fillDBUserWithData($userToUpdate, $user);
			
			$userToUpdate->save();
		}
		
		public function changeLogin($user, $newLogin) {
			$userToUpdate = new DB\SQL\Mapper($this->db, 'users');
			$userToUpdate->load(array('user_id=?', $user->id));
			
			$userToUpdate->user_name = $newLogin;
			$userToUpdate->save();
		}
		
		public function changeEmail($user, $newEmail) {
		
			$userToUpdate = new DB\SQL\Mapper($this->db, 'users');
			$userToUpdate->load(array('user_id=?', $user->id));
			
			$userToUpdate->user_email = $newEmail;
			$userToUpdate->save();
		}
		
		public function activate($user) {
		
			$userToUpdate = new DB\SQL\Mapper($this->db, 'users');
			$userToUpdate->load(array('user_id=?', $user->id));
			
			$userToUpdate->user_active = 1;
			$userToUpdate->save();
		}
		
		public function deactivate($user) {
		
			$userToUpdate = new DB\SQL\Mapper($this->db, 'users');
			$userToUpdate->load(array('user_id=?', $user->id));
			
			$userToUpdate->user_active = 0;
			$userToUpdate->save();
		}
	}
?>