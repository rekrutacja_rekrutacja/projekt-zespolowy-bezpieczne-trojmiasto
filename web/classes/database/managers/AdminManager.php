<?php
	class AdminManager {
	
		private $db;
		
		private function mapDBAdminToAdmin($DBAdmin) {
		
			$admin = new Admin();
			
			$admin->id = $DBAdmin->admin_id;
			$admin->login = $DBAdmin->admin_name;
			$admin->email = $DBAdmin->admin_email;
			$admin->passwordHash = $DBAdmin->admin_password_hash;
			$admin->passwordResetHash = $DBAdmin->admin_password_reset_hash;
			$admin->passwordResetTimestamp = $DBAdmin->admin_password_reset_timestamp;
			$admin->rememberMeToken = $DBAdmin->admin_rememberme_token;
			$admin->failedLogins = $DBAdmin->admin_failed_logins;
			$admin->lastFailedLogin = $DBAdmin->admin_last_failed_login;
			
			return $admin;
		}
		
		private function fillDBAdminWithData($DBAdmin, $modelAdmin) {
			
			$DBAdmin->admin_name = $modelAdmin->login;
			$DBAdmin->admin_email = $modelAdmin->email;
			$DBAdmin->admin_password_hash = $modelAdmin->passwordHash;
			$DBAdmin->admin_password_reset_hash = $modelAdmin->passwordResetHash;
			$DBAdmin->admin_password_reset_timestamp = $modelAdmin->passwordResetTimestamp;
			$DBAdmin->admin_rememberme_token = $modelAdmin->rememberMeToken;
			$DBAdmin->admin_failed_logins = $modelAdmin->failedLogins;
			$DBAdmin->admin_last_failed_login = $modelAdmin->lastFailedLogin;
			
			return $DBAdmin;
		}
		
		
		public function __construct($connection)
		{
			$this->db = $connection;
		}
		
		
		public function add($newAdmin) {
		
			$admin = new DB\SQL\Mapper($this->db,'admin');
			
			$admin->admin_name = $newAdmin->login;
			$admin->admin_email = $newAdmin->email;
			$admin->admin_password_hash = $newAdmin->passwordHash;
			$admin->admin_password_reset_hash = $newAdmin->passwordResetHash;
			$admin->admin_password_reset_timestamp = $newAdmin->passwordResetTimestamp;
			$admin->admin_rememberme_token = $newAdmin->rememberMeToken;
			$admin->admin_failed_logins = $newAdmin->failedLogins;
			$admin->admin_last_failed_login = $newAdmin->lastFailedLogin;

			$admin->save();
			return $admin->admin_id;
		}
		
		public function findByID($id) {
			$retrievedAdmin = new DB\SQL\Mapper($this->db, 'admin');
			$retrievedAdmin->load(array('admin_id=?', $id));
			
			if(is_null($retrievedAdmin->admin_id))
				return null;
			
			return $this->mapDBAdminToAdmin($retrievedAdmin);
		}
		
		public function findByLogin($login) {
		
			$retrievedAdmin = new DB\SQL\Mapper($this->db, 'admin');
			$retrievedAdmin->load(array('admin_name=?', $login));
			
			if(is_null($retrievedAdmin->admin_id))
				return null;
		
			return $this->mapDBAdminToAdmin($retrievedAdmin);
		}
		
		public function findByEmail($email) {
		
			$retrievedAdmin = new DB\SQL\Mapper($this->db, 'admin');
			$retrievedAdmin->load(array('admin_email=?', $email));
			
			if(is_null($retrievedAdmin->admin_id))
				return null;
		
			return $this->mapDBAdminToAdmin($retrievedAdmin);
		}
		
		public function getAll() {
		
			$retrievedAdmin = new DB\SQL\Mapper($this->db, 'admin');
			$retrievedAdmin->load();
			
			if(is_null($retrievedAdmin->admin_id))
				return null;
			
			$admins = array();
			
			do { 
				array_push($admins, $this->mapDBAdminToAdmin($retrievedAdmin)); 
				
			} while($retrievedAdmin->next());
			
			return $admins;
		}
		
		public function deleteByID($id) {
		
			$adminToDelete = new DB\SQL\Mapper($this->db, 'admin');
			$adminToDelete->load(array('admin_id=?', $id));
			
			$adminToDelete->erase();
		}
		
		public function deleteByLogin($login) {
			
			$adminToDelete = new DB\SQL\Mapper($this->db, 'admin');
			$adminToDelete->load(array('admin_name=?', $login));
			
			$adminToDelete->erase();
		}
	
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM admin');
		}
		
		public function update($admin) {
		
			$adminToUpdate = new DB\SQL\Mapper($this->db, 'admin');
			$adminToUpdate->load(array('admin_id=?', $admin->id));
			$adminToUpdate = $this->fillDBAdminWithData($adminToUpdate, $admin);
			
			$adminToUpdate->save();
		}
		
		public function changeLogin($admin, $newLogin) {
		
			$adminToUpdate = new DB\SQL\Mapper($this->db, 'admin');
			$adminToUpdate->load(array('admin_id=?', $admin->id));
			
			$adminToUpdate->admin_name = $newLogin;
			$adminToUpdate->save();
		}
	}
?>