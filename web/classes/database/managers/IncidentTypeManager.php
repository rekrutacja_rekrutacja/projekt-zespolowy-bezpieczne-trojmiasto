<?php
    class IncidentTypeManager {
        
        private $db;
		
		private function mapDBIncidentTypeToIncidentType($DBIncidentType) {
		
			$incidentType = new IncidentType();
			
			$incidentType->id = $DBIncidentType->id;
			$incidentType->name = $DBIncidentType->incident_name;
			$incidentType->type = $DBIncidentType->injury_type;
			$incidentType->weight = $DBIncidentType->incident_weight;
			
			return $incidentType;
		}
		
		private function fillDBIncidentTypeWithData($DBIncidentType, $modelIncidentType) {
		
			$DBIncidentType->incident_name = $modelIncidentType->name;
			$DBIncidentType->injury_type = $modelIncidentType->type;
			$DBIncidentType->incident_weight = $modelIncidentType->weight;
			
			return $DBIncidentType;
		}
		
		
		public function __construct($connection)
		{
			$this->db = $connection;
		}
		
		
		public function add($newIncidentType) {
		
			$incidentType = new DB\SQL\Mapper($this->db, 'incident_type');
			
            $incidentType->incident_name = $newIncidentType->name;
			$incidentType->injury_type = $newIncidentType->type;
			$incidentType->incident_weight = $newIncidentType->weight;
                
            $incidentType->save();
			return $incidentType->id;
		}
		
		public function findByID($id) {
        
            $retrievedIncidentType = new DB\SQL\Mapper($this->db, 'incident_type');
			$retrievedIncidentType->load(array('id=?', $id));
			
			if(is_null($retrievedIncidentType->id))
				return null;
			
			return $this->mapDBIncidentTypeToIncidentType($retrievedIncidentType);
        }
        
        public function findByName($name) {
		
			$retrievedIncidentType = new DB\SQL\Mapper($this->db, 'incident_type');
			$retrievedIncidentType->load(array('incident_name=?', $name));
			
			if(is_null($retrievedIncidentType->id))
				return null;
			
			return $this->mapDBIncidentTypeToIncidentType($retrievedIncidentType);
		}
        
        public function findByType($type) {
		
			$retrievedIncidentType = new DB\SQL\Mapper($this->db, 'incident_type');
			$retrievedIncidentType->load(array('injury_type=?', $type));
			
			if(is_null($retrievedIncidentType->id))
				return null;
			
			$incidentTypes = array();
			
			do { 
				array_push(
					$incidentTypes, 
					$this->mapDBIncidentTypeToIncidentType($retrievedIncidentType)
				); 
				
			} while($retrievedIncidentType->next());
			
			return $incidentTypes;
		}
		
		public function getAll() {
		
			$retrievedIncidentType = new DB\SQL\Mapper($this->db, 'incident_type');
			$retrievedIncidentType->load();
			
			if(is_null($retrievedIncidentType->id))
				return null;
			
			$incidentTypes = array();
			
			do { 
				array_push(
					$incidentTypes, 
					$this->mapDBIncidentTypeToIncidentType($retrievedIncidentType)
				); 
				
			} while($retrievedIncidentType->next());
			
			return $incidentTypes;
		}
		
		public function deleteByID($id) {
		
			$incidentTypeToDelete = new DB\SQL\Mapper($this->db, 'incident_type');
			$incidentTypeToDelete->load(array('id=?', $id));
			
			$incidentTypeToDelete->erase();
		}
		
		public function deleteByName($name) {
		
			$incidentTypeToDelete = new DB\SQL\Mapper($this->db, 'incident_type');
			$incidentTypeToDelete->load(array('incident_name=?', $name));
			
			$incidentTypeToDelete->erase();
		}
	
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM incident_type');
		}
		
		public function update($incidentType) {
		
			$incidentTypeToUpdate = new DB\SQL\Mapper($this->db, 'incident_type');
			$incidentTypeToUpdate->load(array('id=?', $incidentType->id));
			$incidentTypeToUpdate = $this->fillDBIncidentTypeWithData(
				$incidentTypeToUpdate, $incidentType);
			
			$incidentTypeToUpdate->save();
		}
    }
?>