<?php
	require ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/User.php');
	require ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Moderator.php');
	require ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Admin.php');

	class LoginManager {
	
		private $userManager;
		private $moderatorManager;
		private $adminManager;
		
	
		public function __construct($userManager, $moderatorManager, $adminManager)
		{
			$this->userManager = $userManager;
			$this->moderatorManager = $moderatorManager;
			$this->adminManager = $adminManager;
		}
		
		
		public function tryLogIn($login, $passwordHash) {
		
			$logged = new Account();
		
			$account = $this->userManager->findByLogin($login);
			if(!is_null($account)) {
				if($account->passwordHash === $passwordHash) {
					$logged->accountType = 'user';
					$logged->accountObject = $account;
					return $logged;
				}
			}
		
			$account = $this->adminManager->findByLogin($login);
			if(!is_null($account)) {
				if($account->passwordHash === $passwordHash) {
					$logged->accountType = 'admin';
					$logged->accountObject = $account;
					return $logged;
				}
			}
		
			$account = $this->moderatorManager->findByLogin($login);
			if(!is_null($account)) {
				if($account->passwordHash === $passwordHash) {
					$logged->accountType = 'moderator';
					$logged->accountObject = $account;
					return $logged;
				}
			}
			
			return null;
		}
	}
?>