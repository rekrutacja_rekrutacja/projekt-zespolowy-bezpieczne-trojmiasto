<?php

class Registration
{

    public  $registration_successful  = false;

    public  $verification_successful  = false;
   
    public  $errors                   = array();
    
    public  $messages                 = array();
	
	public 	$account				  = null;
	
	public 	$db     				  =	null;
	
	public 	$user					  =	null;
	
	
    public function __construct()
    {
		
		session_start();
		
		$this->db = new DAO();
		$this->user = new User();
		
        if (isset($_POST["register"])) {
			
            $this->registerNewUser($this->db, $this->user, $_POST['user_name'], $_POST['user_email'], $_POST['user_password_new'], $_POST['user_password_repeat'], $_POST["captcha"]);
    
        } else if (isset($_GET["id"]) && isset($_GET["verification_code"])) {
            $this->verifyNewUser($this->db, $this->user, $_GET["id"], $_GET["verification_code"]);
        }
    }

    private function registerNewUser($db, $user, $user_name, $user_email, $user_password, $user_password_repeat, $captcha)
    {

       $user_name  = trim($user_name);
       $user_email = trim($user_email);

        
        if (strtolower($captcha) != strtolower($_SESSION['captcha'])) {
            $this->errors[] = MESSAGE_CAPTCHA_WRONG;
        } elseif (empty($user_name)) {
            $this->errors[] = MESSAGE_USERNAME_EMPTY;
        } elseif (empty($user_password) || empty($user_password_repeat)) {
            $this->errors[] = MESSAGE_PASSWORD_EMPTY;
        } elseif ($user_password !== $user_password_repeat) {
            $this->errors[] = MESSAGE_PASSWORD_BAD_CONFIRM;
        } elseif (strlen($user_password) < 6) {
            $this->errors[] = MESSAGE_PASSWORD_TOO_SHORT;
        } elseif (strlen($user_name) > 64 || strlen($user_name) < 6) {
            $this->errors[] = MESSAGE_USERNAME_BAD_LENGTH;
        } elseif (!preg_match('/^[a-z\d]{6,64}$/i', $user_name)) {
            $this->errors[] = MESSAGE_USERNAME_INVALID;
        } elseif (empty($user_email)) {
            $this->errors[] = MESSAGE_EMAIL_EMPTY;
        } elseif (strlen($user_email) > 64) {
            $this->errors[] = MESSAGE_EMAIL_TOO_LONG;
        } elseif (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = MESSAGE_EMAIL_INVALID;

       
        } else {
            
			
			$this->account = $db->user->findByLogin($user_name);
			$check_email = $db->user->findByEmail($user_email);
			
            if (!is_null($this->account)) {
					
					$this->errors[] = MESSAGE_USERNAME_EXISTS;

            } else if(!is_null($check_email)){
				
				$this->errors[] = MESSAGE_EMAIL_ALREADY_EXISTS;
				
			} else {
               
                $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

                $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));
               
                $user_activation_hash = sha1(uniqid(mt_rand(), true));

                $user->login = $user_name;
				$user->passwordHash = $user_password_hash;
				$user->email = $user_email;
				$user->activationHash = $user_activation_hash;
				$user->registrationDatetime = date("Y-m-d H:i:s");
				$db->user->add($user);
				
       
                $this->account = $db->user->findByLogin($user_name);
				$user_id = $this->account->id;

                if ($user) {
						
						
                    if ($this->sendVerificationEmail($user_id, $user_email, $user_activation_hash)) {

						$this->messages[] = MESSAGE_VERIFICATION_MAIL_SENT;
						
						$this->registration_successful = true;
-						header( "refresh:4;url=http://188.226.171.110/index.php" );
                        
                    } else {
                        
						$db->user->deleteByLogin($user_name);
                        $this->errors[] = MESSAGE_VERIFICATION_MAIL_ERROR;
                    }
                } else {
                    $this->errors[] = MESSAGE_REGISTRATION_FAILED;
					
                }
            }
        }
    }

    public function sendVerificationEmail($user_id, $user_email, $user_activation_hash)
    {
        $mail = new PHPMailer;

        
        if (EMAIL_USE_SMTP) {
           
            $mail->IsSMTP();
         
            $mail->SMTPAuth = EMAIL_SMTP_AUTH;
           
            if (defined(EMAIL_SMTP_ENCRYPTION)) {
                $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
            }
            
            $mail->Host = EMAIL_SMTP_HOST;
            $mail->Username = EMAIL_SMTP_USERNAME;
            $mail->Password = EMAIL_SMTP_PASSWORD;
            $mail->Port = EMAIL_SMTP_PORT;
        } else {
            $mail->IsMail();
        }

        $mail->From = EMAIL_VERIFICATION_FROM;
        $mail->FromName = EMAIL_VERIFICATION_FROM_NAME;
        $mail->AddAddress($user_email);
        $mail->Subject = EMAIL_VERIFICATION_SUBJECT;

        $link = EMAIL_VERIFICATION_URL.'?id='.urlencode($user_id).'&verification_code='.urlencode($user_activation_hash);

        
        $mail->Body = EMAIL_VERIFICATION_CONTENT.' '.$link;

        if(!$mail->Send()) {
            $this->errors[] = MESSAGE_VERIFICATION_MAIL_NOT_SENT;
            return false;
        } else {
            return true;
        }
    }

   
    public function verifyNewUser($db, $user, $user_id, $user_activation_hash)
    {
			$this->account = $db->user->findByID($user_id);
			
			$this->account->isActive = 1;
			$this->account->activationHash = NULL;
			
			$db->user->update($this->account);
			
			$check = $db->user->findByID($user_id);
			
            if ($check->activationHash == NULL && $check->isActive == 1) {
				
                $this->verification_successful = true;
                $this->messages[] = MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL;
				header( "refresh:4;url=http://188.226.171.110/login.php?link=3" );
				
            } else {
				
                $this->errors[] = MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL;
            }
        
    }
}
